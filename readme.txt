=== Plugin Name ===
Contributors: crystalraebryant
Tags: habitica, habitrpg
Requires at least: 3.0.1
Tested up to: 4.4
Stable tag: 4.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add a widget to the your site showing the current status of your Habitica character.

== Description ==

Add a widget to the your site showing the current status of your [Habitica](http://habitica.com) character. Displays mount, pet, equipment, level, health, and mana. 

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. In Appearance->Widgets, drag the Habitica User Widget to the desired widget area.
1. Title is optional, and will appear above the user sprite image.
1. User ID and API Key need to be copied from your [Habitica API Settings](https://habitica.com/#/options/settings/api). They will be stored in the database.


== Frequently Asked Questions ==

= How do I find my Habitica User ID and API Key? =

If you're already logged into Habitica, [click here](https://habitica.com/#/options/settings/api).

== Screenshots ==

1. Sample Widget
2. Widget Settings

== Changelog ==

= 1.0 =
* Initial update. Based on code from [gersande](https://gitlab.com/gersande/habitrpg-widget), which is based on code from [pipcorona](https://github.com/pipcorona/HabitRPG-widget).

== Future Improvements ==

* Reduce the size of the plugin. It's very image heavy.
* Give user option to display username or not.
* Add shortcode option so that user can choose to display user widget within a page or post.